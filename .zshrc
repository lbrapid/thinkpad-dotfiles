# Load environment variables
source ~/.zsh/environment.zsh

# Load terminal configuration
source ~/.zsh/title.zsh
source ~/.zsh/prompt.zsh

# Load plugins
source ~/.zsh/prezto.zsh
source ~/.zsh/antigen.zsh

# Load custom configurations
source ~/.zsh/aliases.zsh
