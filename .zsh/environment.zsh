export TERMINAL="kitty"
export EDITOR="nvim"
export VISUAL="nvim"
export DIFFPROG="nvim -d"
export MANPAGE="nvim -c 'set ft=man' -"
export LANG=en_US.UTF-8

# My own binaries
export PATH="$HOME/bin:$PATH"
