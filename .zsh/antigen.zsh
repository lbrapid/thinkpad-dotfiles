source /usr/share/zsh/share/antigen.zsh

antigen use prezto

antigen bundles <<EOB
  Tarrasch/zsh-bd
  zsh-users/zsh-syntax-highlighting
  zsh-users/zsh-history-substring-search
  mafredri/zsh-async
EOB

antigen theme https://github.com/denysdovhan/spaceship-prompt spaceship

antigen apply
