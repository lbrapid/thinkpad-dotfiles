# General
alias h="history"
alias l="less"
alias k="clear"
alias rmf="rm -rf"
alias grep="grep --color"
alias mkdir="mkdir -p"
alias sudo="sudo -E "

# Editor
alias e="nvim"
alias vi="nvim"
alias vim="nvim"

# Listing files
alias ll="ls -l"
alias la="ll -a"
alias lk="ll -s=size"                # Sorted by size
alias lm="ll -s=modified"            # Sorted by modified date
alias lc="ll --created -s=created"   # Sorted by created date

# Git
alias ga='git add'
alias gaa='git add --all'
alias gau='git add --update'
alias gb='git branch'
alias gap='git add --patch'
alias gc='git commit -v'
alias gca='git commit -a'
alias gco='git checkout'
alias gd='git diff'
alias gdc='git dc'
alias gs='git status --short'
alias gsl='git status'
alias gpp='git pp'
alias gup='git smart-pull'
alias pull='git pull --rebase'
alias glg='git lg'
alias grh='git reset HEAD'
alias grc='git rebase --continue'
alias grs='git rebase --skip'
alias gra='git rebase --abort'
alias sha='git rev-parse HEAD | sed ":a;N;$!ba;s/\n//g" | pbcopy'
alias cherry='git cherry-pick $1'
alias kdf='git difftool -y -t Kaleidoscope'
alias kmg='git mergetool -y -t Kaleidoscope'
alias gln='git log -n'
alias gst='git stash'
alias good='git bisect good'
alias bad='git bisect bad'

# Tmux
alias ta='tmux attach -t'
alias ts='tmux new-session -s'
alias tls='tmux list-sessions'
alias tks='tmux kill-session'
alias mux='tmuxinator'
