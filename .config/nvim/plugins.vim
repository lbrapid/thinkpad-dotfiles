if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=/home/johnd/.cache/dein/repos/github.com/Shougo/dein.vim

" Required:
if dein#load_state('/home/johnd/.cache/dein')
  call dein#begin('/home/johnd/.cache/dein')

  " Let dein manage dein
  " Required:
  call dein#add('/home/johnd/.cache/dein/repos/github.com/Shougo/dein.vim')

  " Look & Feel
  call dein#add('joshdick/onedark.vim')                           " Color theme
  call dein#add('itchyny/lightline.vim')                          " Bottom bar
  call dein#add('mgee/lightline-bufferline')                      " Top bar
  call dein#add('cskeeters/vim-smooth-scroll')                    " Smooth scroll

  " Format code
  call dein#add('tpope/vim-sleuth')                               " Automatically detect tabs vs spaces
  call dein#add('sbdchd/neoformat')                               " Automatically format code

  " Manipulate code
  call dein#add('tpope/vim-repeat')                               " Repeat for plugins
  call dein#add('tpope/vim-surround')                             " Surround
  call dein#add('tpope/vim-abolish')                              " Susbstitute with Smart Case (:S//)
  call dein#add('tpope/vim-commentary')                           " Comment code
  call dein#add('tpope/vim-endwise')                              " Automatically put 'end' in some languages
  call dein#add('Raimondi/delimitMate')                           " Insert closing brackets automatically
  call dein#add('alvan/vim-closetag')                             " Automatically put closing tag in XML
  call dein#add('junegunn/vim-easy-align')                        " Easy align around equals
  call dein#add('matze/vim-move')                                 " Move blocks of code
  call dein#add('mattn/emmet-vim')                                " Emmet for vim

  " Targets and text objects
  call dein#add('wellle/targets.vim')                             " Add more targets to operate on

  " Navigate code
  call dein#add('haya14busa/incsearch.vim')                       " Incremental search
  call dein#add('haya14busa/incsearch-fuzzy.vim')                 " Fuzzy incremental search
  call dein#add('osyo-manga/vim-anzu')                            " Show search count
  call dein#add('justinmk/vim-sneak')                             " Search for next ocurrence with 2 characters
  call dein#add('farmergreg/vim-lastplace')                       " Restore cursor position

  " Navigate files and buffers
  call dein#add('tpope/vim-vinegar')                              " Improved netrw listing and navigation
  call dein#add('junegunn/fzf', {'build': './install --bin'})     " Fuzzy search - binary
  call dein#add('junegunn/fzf.vim')                               " Fuzzy search - vim plugin
  call dein#add('justinmk/vim-sneak')                             " Search for next ocurrence with 2 characters
  call dein#add('benizi/vim-automkdir')                           " Automatically create missing folders on save

  " Git
  call dein#add('tpope/vim-fugitive')                             " Git integration
  call dein#add('airblade/vim-gitgutter')                         " Git gutter

  " Render code
  call dein#add('sheerun/vim-polyglot')                           " Many many syntaxes
  call dein#add('ap/vim-css-color')                               " Colors in CSS

  " Required:
  call dein#end()
  call dein#save_state()
endif

" Required:
filetype plugin indent on
syntax enable
