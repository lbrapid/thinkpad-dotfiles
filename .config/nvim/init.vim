source $HOME/.config/nvim/general.vim
source $HOME/.config/nvim/plugins.vim
source $HOME/.config/nvim/appearance.vim
source $HOME/.config/nvim/mappings.vim
source $HOME/.config/nvim/fzf.vim
